(defpackage #:euphony
  (:use #:cl)
  (:export
   #:play
   #:make-euphony 
   #:launch-euphony
   #:close-euphony
   #:volume
   #:seek
   #:rolloff
   #:stop
   #:clear
   #:poweron
   #:shutdown
   #:clear
   #:rejuvenate)) 

(in-package :euphony)

(defclass euphony ()
  ((eid)
   (command)
   (notify)
   (acknowledge) 
   (command-name)
   (notify-name)
   (acknowledge-name)
   (crashes)
   (eproc)
   (input)))

(defmethod on-end ((id t)))

(defun make-euphony (id &key (instance (make-instance 'euphony)))
  (with-slots (command-name command notify-name notify acknowledge-name acknowledge crashes input eproc eid) instance
    (setf
     eid id
     crashes 0
     command-name (format nil "/~a-~a-euphony-command" id (sb-posix:getpid)) 
     command (mqueue:open-queue command-name)
     acknowledge-name (format nil "/~a-~a-euphony-acknowledge" id (sb-posix:getpid)) 
     acknowledge (mqueue:open-queue acknowledge-name :non-blocking t :max-msgs 1)
     notify-name (format nil "/~a-~a-euphony-notify" id (sb-posix:getpid)) 
     notify (mqueue:open-queue notify-name :non-blocking t :max-msgs 1)) 
    (mqueue:flush command)
    (mqueue:flush acknowledge)
    (mqueue:flush notify)
    ;; hopefully clean up queues
    (push (let ((cleanup1 (list command-name acknowledge-name notify-name))
                (cleanup2 (list command acknowledge notify)))
            (lambda ()
              (map nil (lambda (x) (ignore-errors (mqueue:unlink-queue x))) cleanup1)
              (map nil (lambda (x) (ignore-errors (mqueue:close-queue x))) cleanup2)))
          sb-ext:*exit-hooks*)
    (if (and (boundp 'input) (bt:threadp input) (bt:thread-alive-p input))
        (bt:destroy-thread input)) 
    (setf
     input (bt:make-thread
            (lambda ()
              (loop for wrb = (mqueue:receive-message notify) 
                    do (let* ((tkns (str:split " " wrb))
                              (header (car tkns)))
                         (declare (dynamic-extent tkns))
                         (cond
                           ((equalp "(end" (car tkns))
                            (on-end (parse-integer (cadr tkns))))))))
            :name (format nil "euphony receive channel for ~a" eid))))
  ;; note: call launch-euphony or something now
  instance)

(defun launch-euphony (euphony)
  "Note: this is blocking and should be done in a thread"
  (with-slots (eid eproc crashes) euphony

    ;; Launch the program (and block)
    (setq eproc
          (uiop:launch-program
           (format nil "sbcl --disable-debugger --eval \"(load \\\"~a\\\")\" ~a ~a \"~a\"" ; --eval '(quit)'
                   (asdf:system-relative-pathname :euphony-harmony "launcher.lisp")
                   eid
                   (sb-posix:getpid) 
                   (asdf:system-relative-pathname :euphony-harmony "euphony-client.lisp"))))
    (let ((e2 eproc))
      (push (lambda () (ignore-errors (uiop:terminate-process e2))) sb-ext:*exit-hooks*))))

(defun stop-euphony (euphony) 
  "Note: if a resurrector is active it will resurrect the program"
  (with-slots (eid command-name command notify-name notify acknowledge-name acknowledge eproc) euphony
    (when (and (eq 'UIOP/LAUNCH-PROGRAM::PROCESS-INFO eproc)
               (uiop:process-alive-p eproc))
      (uiop:terminate-process eproc)    
      (sleep 0))
    (values)))

(defun close-euphony (euphony)
  (with-slots (eid command-name command notify-name notify acknowledge-name acknowledge eproc) euphony
    (stop-euphony euphony) 
    (mqueue:send-message command "(quit)")
    (ignore-errors (mqueue:unlink-queue command-name))
    (ignore-errors (mqueue:unlink-queue acknowledge-name))
    (ignore-errors (mqueue:unlink-queue notify-name))
    (ignore-errors (mqueue:close-queue command))
    (ignore-errors (mqueue:close-queue notify))
    (ignore-errors (mqueue:close-queue acknowledge))
    (setq command nil notify nil acknowledge nil)
    (values)))

(defun play (euphony source &key (name nil np) (class 'voice cp) (mixer :effect mp) (effects nil ep) (server nil sep) (repeat nil rpp)
                              (repeat-start 0 rsp) (on-end :free op) (location nil lp) (velocity nil vep) (volume 1.0 vlp)
                              (if-exists :error ip) (synchronize nil syp) (reset nil rep)

                              (seconds-of-patience 0.5))
  (with-slots (command acknowledge crashes eid) euphony
    ;; this shoudl be timed.
    (mqueue:send-message command 
                         (with-output-to-string (s)
                           (format s "(play ")
                           (format s "\"~a\"" source) 
                           (if np (format s " :name ~a" name))
                           (if cp (format s " :class ~a" class))
                           (if mp (format s " :mixer ~a" mixer))
                           (if ep (format s " :effects ~a" effects))
                           (if sep (format s " :server ~a" server))
                           (if rpp (format s " :repeat ~a" repeat))
                           (if rsp (format s " :repeat-start ~a" repeat-start))
                           (if op (format s " :on-end ~a" t))
                           (if lp (format s " :location ~a" location))
                           (if vep (format s " :velocity ~a" velocity))
                           (if vlp (format s " :volume ~a" volume))
                           (if ip (format s " :if-exists ~a" if-exists))
                           (if syp (format s " :synchronize ~a" synchronize))
                           (if rep (format s " :reset ~a" reset))
                           (format s ")")      
                           s))
    ;; if it doesn't receive, could be a 
    (mqueue:receive-message-timed acknowledge :timeout seconds-of-patience)))

(defun volume (euphony volume)
  (declare (type float volume))
  (with-slots (command) euphony 
    (mqueue:send-message command (format nil "(volume ~a)" volume))))

(defun seek (euphony seek) 
  (declare (type float seek))
  (with-slots (command) euphony 
    (mqueue:send-message command (format nil "(seek ~a)" seek))))

(defun rolloff (euphony rolloff) 
  (declare (type float rolloff))
  (with-slots (command) euphony 
    (mqueue:send-message command (format nil "(rolloff ~a)" rolloff))))

(defun stop (euphony voice-id) 
  (declare (type integer voice-id))
  (with-slots (command) euphony 
    (mqueue:send-message command (format nil "(stop ~a)" voice-id)))) 

(defun poweron (euphony)
  (with-slots (command) euphony 
    (mqueue:send-message command "(poweron)")))

(defun shutdown (euphony) 
  (with-slots (command) euphony 
    (mqueue:send-message command "(shutdown)")))

(defun clear (euphony)
  (with-slots (command) euphony 
    (mqueue:send-message command "(clear)")))

(defun rejuvenate (euphony)
  (with-slots (command) euphony 
    (mqueue:send-message command "(rejuvenate)"))) 

;; NOTE: anything to do with polling could use the shm shared memory library.
