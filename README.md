 # What

Separates harmony-simple into another process, controlled from your main program.
IPC is done by cl-posix-mqueue.
Some code is there to try and kill the euphony client when your main program closes using multiple tactics from both processes because for some reason linux doesn't provide proper mechanism for that.

## Usage

     (asdf:load-system :euphony-harmony)
     (defvar *euphony-instance* (euphony:start "some-name")) ; starts the client program
     (defvar *some-song* (euphony:play *euphony-instance* "some-song.mp3"))
     ;; *some-song* will be a string
     (euphony:volume *euphony-instance* 0.6)
     (sleep 3)
     (euphony:stop *euphony-instance* *some-song*)
     (euphony:close-euphony *euphony-instance*) ; kills the client program

## Building
player can be built with

    (progn
    (asdf:load-system :cl-posix-mqueue)
    (ql:quickload :harmony)
    (ql:quickload :cl-destruction)
    (ql:quickload :str)
    (load "euphony-client.lisp")
    (sb-ext:save-lisp-and-die #P"~/euphony.client"
    :toplevel #'init
    :executable t
    :compression t))

