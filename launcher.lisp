(asdf:load-system :cffi)
(asdf:load-system :mqueue)

(defvar *program* nil)
(defvar *name* (or (cadr *posix-argv*) (write-to-string (read))))
(defvar *pid* (caddr *posix-argv*))
(defvar *script-loc* (or (cadddr *posix-argv*) (format nil "~a" (asdf:system-relative-pathname :euphony-harmony "euphony-client.lisp"))))
;; (defvar *controller-pid* (handler-bind (parse-integer (cadddr *posix-argv*)) (error () (read))))
;; (loop while (not (numberp *pid*))
;;       do (makunbound '*pid*)
;;          (when (not (numberp *pid*)) 
;;            (progn *pid*))
;;          ;; (restart-case (error "need pid")
;;          ;;   (store-value (value)
;;          ;;     :report "Specify a pid."
;;          ;;     (setf (symbol-value value) value)))
;;       )
;; (if (not (numberp *pid*))
;;     (makunbound '*pid*))
;; *pid*
(cond
  ((null *pid*) (makunbound '*pid*) *pid*)
  ((stringp *pid*) (setq *pid* (parse-integer *pid*))))

(defmacro set-signal-handler (signo &body body)
  (let ((handler (gensym "HANDLER")))
    `(progn
       (cffi:defcallback ,handler :void ((signo :int))
         (declare (ignore signo))
         ,@body)
       (cffi:foreign-funcall "signal" :int ,signo :pointer (cffi:callback ,handler)))))

(labels ((signal-app-to-quit ()
           ;; (format t "Quitting lol!!!11")
           (uiop:run-program "touch /tmp/euphony-launcher.m4d")
           (ignore-errors (uiop:terminate-process *program*))
           (exit)))
  (set-signal-handler 2 (signal-app-to-quit))
  (set-signal-handler 3 (signal-app-to-quit))
  (set-signal-handler 8 (signal-app-to-quit))
  (set-signal-handler 9 (signal-app-to-quit))
  (set-signal-handler 14 (signal-app-to-quit))
  (set-signal-handler 15 (signal-app-to-quit)))

(defun destroy ()
  (ignore-errors (uiop:terminate-process *program*))
  (ignore-errors (mqueue:unlink-queue (format nil "/~a-~a-euphony-command" *name* *pid*)))
  (ignore-errors (mqueue:unlink-queue (format nil "/~a-~a-euphony-acknowledge" *name* *pid*)))
  (ignore-errors (mqueue:unlink-queue (format nil "/~a-~a-euphony-notify" *name* *pid*)))
  (exit))

(bt:make-thread
 (lambda () (loop do (sleep 2)
                (format t "active

controller ~A (~a)
resurrector ~A (~a)
player ~A (~a)

" *pid* (ignore-errors (sb-posix:kill *pid* -0) t)
(sb-posix:getpid) (ignore-errors (sb-posix:kill (sb-posix:getpid) -0) t)
(uiop:process-info-pid *program*) (ignore-errors (sb-posix:kill (uiop:process-info-pid *program*) -0) t))
                (if (eq 'uiop/launch-program::process-info (type-of *program*))
                    (uiop:process-info-pid *program*))
                ;;cool, but does not work in busybox.
                ;;when (not (equalp parentpid (sb-posix:getppid))) 
                (handler-case (sb-posix:kill *pid* -0)
                  (sb-posix:syscall-error
                    ()
                    (destroy))))))
(loop for i from 0
      do (handler-case (sb-posix:kill *pid* -0)
           (sb-posix:syscall-error
             ()
             (destroy)))
         (setq *program*
               ;; (uiop:launch-program "sleep 300")(uiop:run-program "touch /tmp/euphony-launcher.m4d")
               
               ;; (uiop:launch-program
               ;;  "sbcl --disable-debugger --eval '(push (lambda () (uiop:run-program \"touch /tmp/euphony-launcher.m4d\")) sb-ext::*exit-hooks*)'") 
               (uiop:launch-program
                (format nil "sbcl --disable-debugger --eval '(load \"~a\")' --eval \"(init)\" ~a ~a"
                        *script-loc*
                        *name*
                        *pid*)))
         (ignore-errors (uiop:wait-process *program*))
         (if (evenp i) (sleep 3)))

