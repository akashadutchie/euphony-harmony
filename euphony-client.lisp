(require 'asdf)
(asdf:load-system :serapeum)
(asdf:load-system :mqueue)
(asdf:load-system :harmony)
(asdf:load-system :cl-destruction)
(asdf:load-system :str)

(defvar *command-queue*) ; input
(defvar *acknowledge-queue*) ; acknowledge commands
(defvar *notify-queue*) ; output

(defvar *command-queue-name*) ; input
(defvar *acknowledge-queue-name*) ; acknowledge commands
(defvar *notify-queue-name*) ; output
(defvar *voice-counter* -1)
(defvar *voice-db* (make-hash-table))
(defvar id)
(defvar pid)
;; (defvar ppid)
(defvar debug-mode nil)

(defun cleanup ()
  ;; (ignore-errors (mqueue:unlink-queue *command-queue-name*))
  ;; (ignore-errors (mqueue:unlink-queue *acknowledge-queue-name*))
  ;; (ignore-errors (mqueue:unlink-queue *notify-queue-name*))
  (ignore-errors (mqueue:close-queue *command-queue*))
  (ignore-errors (mqueue:close-queue *acknowledge-queue*))
  (ignore-errors (mqueue:close-queue *notify-queue*)))

(defun init ()
  (org.shirakumo.fraf.harmony.user:maybe-start-simple-server)
  (when (not (boundp 'id))
    (setq id (cadr *posix-argv*)))
  (when (not (boundp 'pid))
    (setq pid (caddr *posix-argv*))
    (if (not (numberp pid))
        (setf pid (parse-integer pid))))
  ;; (when (not (boundp 'ppid))
  ;;   (setq ppid (sb-posix:getppid))
  ;;   (if (not (numberp ppid))
  ;;       (setf pid (parse-integer ppid))))
  ;; (format t "id: ~a pid: ~a ppid: ~a" id pid ppid)
  (setq
   *command-queue-name* (format nil "/~a-~a-euphony-command" id pid)
   *acknowledge-queue-name* (format nil "/~a-~a-euphony-acknowledge" id pid)
   *notify-queue-name* (format nil "/~a-~a-euphony-notify" id pid) 
   *command-queue* (mqueue:open-queue *command-queue-name*)
   *acknowledge-queue* (mqueue:open-queue *acknowledge-queue-name* :non-blocking t :max-msgs 1) 
   *notify-queue* (mqueue:open-queue *notify-queue-name* :non-blocking t :max-msgs 1))

  ;; do not unlink after an abnormal exit
  (push (let ( ;(cleanup1 (list *command-queue-name* *acknowledge-queue-name* *notify-queue-name*))
              (cleanup2 (list *command-queue* *acknowledge-queue* *notify-queue*)))
          (lambda ()
            ;; (map nil (lambda (x) (ignore-errors (mqueue:unlink-queue x))) cleanup1)
            (map nil (lambda (x) (ignore-errors (mqueue:close-queue x))) cleanup2)))
        sb-ext:*exit-hooks*)

  (bt:make-thread
   (lambda () (loop do (sleep 2)
                  ;; doesn't really work in busybox.
                  ;; also where's the call to cleanup??                  
                  ;; (when (not (equalp ppid (sb-posix:getppid)))
                  ;;   (ignore-errors (uiop:terminate-process *program*))
                  ;;   (exit))
                  (handler-case (sb-posix:kill pid -0)
                    (sb-posix:syscall-error
                      ()
                      (cleanup)
                      (exit)))))) 
  
  (handler-case 
      (loop
        do (if debug-mode (format t "
waiting on ~a, with outs ~a and ~a" *command-queue* *acknowledge-queue* *notify-queue*)) 
           (let ((rmsg (mqueue:receive-message-timed *command-queue* :timeout 2)))
             (if debug-mode (format t "
ping ~a" rmsg))
             (when (and (stringp rmsg)
                        (str:starts-with? "(" rmsg)
                        (str:ends-with? ")" rmsg))
               (let* ((tkns (str:split " " rmsg))
                      (header (car tkns)))
                 (declare (dynamic-extent tkns))
                 (when debug-mode
                   (format t "
~a: ~a" header tkns))
                 (if header
                     (symbol-macrolet ((rest-msg (format nil "(~a" (subseq rmsg (+ 1 (position #\  rmsg :test #'equalp))))))
                       (cond
                         ((equalp "(play" header)
                          (when debug-mode
                            (format t "
playing ~a" (car (read-from-string rest-msg)))) 
                          (handler-case
                              (let* ((rest-tokens (read-from-string rest-msg))
                                     (voice (apply #'org.shirakumo.fraf.harmony:play
                                                   (merge-pathnames (car rest-tokens))
                                                   :on-end
                                                   (lambda (voice)
                                                     (org.shirakumo.fraf.harmony:with-server (org.shirakumo.fraf.harmony:*server* :synchronize NIL)
                                                       (org.shirakumo.fraf.harmony:disconnect voice T)
                                                       (org.shirakumo.fraf.mixed:withdraw voice T)
                                                       (org.shirakumo.fraf.mixed:seek voice 0)) 
                                                     (remhash *voice-counter* *voice-db*)
                                                     (if (member ":on-end" (cdr rest-tokens) :test #'equalp)
                                                         (mqueue:f-message *notify-queue* (format nil "(end ~a)" *voice-counter*))))
                                                   (cl-destruction:delete-prop ":on-end" (cdr rest-tokens) :test #'equalp)))) 
                                (declare (dynamic-extent rest-tokens))
                                (incf *voice-counter*) 
                                (setf (gethash *voice-counter* *voice-db*) voice)
                                (mqueue:f-message *acknowledge-queue* (format nil "~a" *voice-counter*)))
                            (error (condition) (mqueue:f-message *notify-queue* (string (type-of condition))))))
                         ((equalp "(create" header)
                          (apply #'org.shirakumo.fraf.harmony:create (read-from-string rest-msg)))
                         ((equalp "(stop" header)
                          (org.shirakumo.fraf.harmony:stop (gethash (parse-integer (cadr tkns) :junk-allowed t) *voice-db*)))
                         ((equalp "(shutdown)" header)
                          (org.shirakumo.fraf.harmony:stop org.shirakumo.fraf.harmony:*server*))
                         ((equalp "(poweron)" header)
                          (org.shirakumo.fraf.harmony:maybe-start-simple-server))
                         ((equalp "(rejuvenate)" header)
                          (org.shirakumo.fraf.harmony:stop org.shirakumo.fraf.harmony:*server*)
                          (org.shirakumo.fraf.harmony:maybe-start-simple-server))
                         ((equalp "(clear)" header)
                          (org.shirakumo.fraf.harmony:clear org.shirakumo.fraf.harmony.user:*server*))
                         ((equalp "(seek" header)
                          (apply #'org.shirakumo.fraf.harmony.user:seek (read-from-string rest-msg)))
                         ((equalp "(rolloff" header)
                          (setf (org.shirakumo.fraf.mixed:volume org.shirakumo.fraf.harmony.user:*server*) (serapeum:parse-float (cadr tkns) :type 'double-float :junk-allowed t)))
                         ((equalp "(volume" header)
                          (setf (org.shirakumo.fraf.mixed:volume org.shirakumo.fraf.harmony.user:*server*) (serapeum:parse-float (cadr tkns) :type 'double-float :junk-allowed t)))
                         ((equalp "(quit)" header)
                          (cleanup) 
                          (return)))))))))
    (error (cnd)
      (if debug-mode (format t "
closed due to ~a" cnd))
      (org.shirakumo.fraf.harmony:clear org.shirakumo.fraf.harmony:*server*)
      (org.shirakumo.fraf.harmony:stop org.shirakumo.fraf.harmony:*server*)
      (ignore-errors (mqueue:close-queue *command-queue*))
      (ignore-errors (mqueue:close-queue *acknowledge-queue*))
      (ignore-errors (mqueue:close-queue *notify-queue*)))))


